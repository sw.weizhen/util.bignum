package bncalculator

import (
	"errors"
	"regexp"
	"strings"
)

func regxpVerify(num string) (bool, error) {
	regx, err := regexp.Compile(REGEX_NUMBER)
	if err != nil {
		return false, err
	}

	matchVal := regx.FindString(num)

	if matchVal != num {
		return false, nil
	}

	return true, nil
}

func typedNum(x string) (string, error) {
	x = strings.TrimSpace(x)
	if len(x) == 0 {
		return EMPTY, errors.New("invalid argument")
	}

	ok, err := regxpVerify(x)
	if err != nil {
		return EMPTY, err
	}
	if !ok {
		return EMPTY, errors.New("illegal argument")
	}

	return trimNum(x), nil
}

func verifyArgument(x, y string) (string, string, error) {
	var e error
	x, e = typedNum(x)
	if e != nil {
		return NOT_A_NUMBER, NOT_A_NUMBER, e
	}

	y, e = typedNum(y)
	if e != nil {
		return NOT_A_NUMBER, NOT_A_NUMBER, e
	}

	return x, y, nil
}
