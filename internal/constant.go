package bncalculator

const (
	REGEX_NUMBER          = `[+-]?\d+(\.\d*)?|[+-]?\.\d+|[+-]?\.`
	REGEX_SCIENTIFIC_FORM = `\d[.]?\d*[Ee][+-]\d+`
)

const (
	SUM        = 1
	DIFFERENCE = -1
)

const (
	HEX_PLUSSIGN   = 0x2b
	HEX_MINUSSIGN  = 0x2d
	HEX_MULTIPLIED = 0x2a
	HEX_DIVIDED    = 0x2f
	HEX_POINT      = 0x2e
	HEX0           = 0x30
	HEX1           = 0x31
	HEX2           = 0x32
	HEX3           = 0x33
	HEX4           = 0x34
	HEX5           = 0x35
	HEX6           = 0x36
	HEX7           = 0x37
	HEX8           = 0x38
	HEX9           = 0x39
)

const (
	CHAR0 = '0'
	CHAR1 = '1'
	CHAR2 = '2'
	CHAR3 = '3'
	CHAR4 = '4'
	CHAR5 = '5'
	CHAR6 = '6'
	CHAR7 = '7'
	CHAR8 = '8'
	CHAR9 = '9'
)

const (
	STR0 = "0"
	STR1 = "1"
	STR2 = "2"
	STR3 = "3"
	STR4 = "4"
	STR5 = "5"
	STR6 = "6"
	STR7 = "7"
	STR8 = "8"
	STR9 = "9"
)

const (
	PLUS         = "+"
	MINUS        = "-"
	DECIMALPOINT = "."
	NIL          = "0"
	EMPTY        = ""
	NILH         = "0."
)

const (
	PREFIX = 1
	SUFFIX = -1
)

const (
	GREATER = 1
	EQUAL   = 0
	LESS    = -1
	OBSCURE = -9
)

const (
	NOT_A_NUMBER = "NaN"
	INFINITY     = "Infinity"
)

const (
	PANIC_DIVIDED_BY_0          = "divided by 0"
	PANIC_OVERFLOW_Q_R          = "overflow in quotient and remainder"
	PANIC_CONVERT_BN_FAIL       = "fail to convert %s to big number"
	PANIC_CONVERT_BN_FRACTIONAL = "fail to convert %s to big number: out of range fractional part"
)
