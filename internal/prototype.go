package bncalculator

import (
	"fmt"
	"strings"
)

func sum(x, y string) []int {

	x, y = alignNil(x, y)

	return sumUnsafety(x, y)
}

func sumUnsafety(x, y string) []int {

	sm := make([]int, len(x)+1)

	carry := 0
	for i := len(x) - 1; i >= 0; i-- {
		s := int((x[i]-HEX0)+(y[i]-HEX0)) + carry

		carry = 0

		fS := s
		if s > 9 {
			fS = s % 10
			carry = 1
		}

		sm[i+1] = fS
	}

	if carry != 0 {
		sm[0] = 1
	}

	return sm
}

func difference(x, y string) []int {

	x, y = alignNil(x, y)

	return differenceUnsafety(x, y)
}

func differenceUnsafety(x, y string) []int {

	diff := make([]int, len(x))

	borrow := 0
	m := 0
	n := 0
	for i := len(x) - 1; i >= 0; i-- {
		m = int(x[i] - HEX0)
		n = int(y[i] - HEX0)

		if borrow == 1 {
			m -= 1
		}

		if m >= n {
			borrow = 0

		} else {
			m += 10
			borrow = 1
		}

		diff[i] = m - n
	}

	return diff
}

func product(x, y string) []int {

	prod := make([]int, len(x)+len(y))

	for i := 0; i < len(x); i++ {
		for j := 0; j < len(y); j++ {
			prod[i+j+1] += int((x[i] - HEX0) * (y[j] - HEX0))
		}
	}

	for i := len(prod) - 1; i > 0; i-- {
		prod[i-1] += prod[i] / 10
		prod[i] = prod[i] % 10
	}

	if prod[0] == 0 {
		prod = prod[1:]
	}

	return prod
}

func deterSumFunc(x, y string) (string, error) {
	var err error
	pos := -1

	isXMinus := x[0] == HEX_MINUSSIGN
	isYMinus := y[0] == HEX_MINUSSIGN
	isXYMinus := isXMinus && isYMinus

	x = strings.ReplaceAll(x, MINUS, EMPTY)
	y = strings.ReplaceAll(y, MINUS, EMPTY)

	x, y, pos, err = alignPosition(x, y)
	if err != nil {
		return NOT_A_NUMBER, err
	}

	if (isYMinus != isXMinus) && x == y {
		return NIL, nil
	}

	// ++ --
	if (!isXMinus && !isYMinus) || isXYMinus {
		val, err := SumNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if pos > 0 {
			val = insertPoint(val, pos)
		}

		if isXYMinus {
			val = MINUS + val
		}

		return val, nil
	}

	// +-
	if !isXMinus && isYMinus {
		val, err := DifferenceNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if pos > 0 {
			if val[0] == HEX_MINUSSIGN {
				val = MINUS + insertPoint(val[1:], pos)

			} else {
				val = insertPoint(val, pos)

			}
		}

		return val, nil
	}

	// -+
	if isXMinus && !isYMinus {
		val, err := DifferenceNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if val[0] == HEX_MINUSSIGN {
			val = val[1:]
		}

		if pos > 0 {
			val = insertPoint(val, pos)
		}

		compare, err := compareAligned(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if compare == 1 {
			val = MINUS + val
		}

		return val, nil
	}

	return NOT_A_NUMBER, nil
}

func deterDiffFunc(x, y string) (string, error) {

	var err error
	pos := -1

	isXMinus := x[0] == HEX_MINUSSIGN
	isYMinus := y[0] == HEX_MINUSSIGN
	isXYMinus := isXMinus && isYMinus

	x = strings.ReplaceAll(x, MINUS, EMPTY)
	y = strings.ReplaceAll(y, MINUS, EMPTY)

	x, y, pos, err = alignPosition(x, y)
	if err != nil {
		return NOT_A_NUMBER, err
	}

	// -1 - -1, 1 - 1
	if (isYMinus == isXMinus) && x == y {
		return NIL, nil
	}

	fmt.Printf("x: %v\ny: %v\npos: %v\n", x, y, pos)

	compare, e := compareAligned(x, y)
	if e != nil {
		return NOT_A_NUMBER, err
	}

	// ++ --
	if !isXMinus && !isYMinus || isXYMinus {
		val, err := DifferenceNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if val[0] == HEX_MINUSSIGN {
			val = val[1:]
		}

		if pos > 0 {
			val = insertPoint(val, pos)
		}

		if !isXYMinus && compare == -1 {
			val = MINUS + val
		}

		if isXYMinus && compare == 1 {
			val = MINUS + val
		}

		fmt.Printf("+ - + => %v\n", val)

		return val, nil
	}

	// +-
	if !isXMinus && isYMinus {
		val, err := SumNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if pos > 0 {
			val = insertPoint(val, pos)
		}

		fmt.Printf("+ - - => %v\n", val)

		return val, nil
	}

	// -+
	if isXMinus && !isYMinus {
		val, err := SumNatural(x, y)
		if err != nil {
			return NOT_A_NUMBER, err
		}

		if pos > 0 {
			val = insertPoint(val, pos)
		}

		val = MINUS + val
		fmt.Printf("- - + => %v\n", val)

		return val, nil
	}

	return NOT_A_NUMBER, nil
}
