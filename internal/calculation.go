package bncalculator

import (
	"fmt"
	"strings"
)

func Sum(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	val, err := deterSumFunc(x, y)
	if err != nil {
		return NOT_A_NUMBER, err
	}

	return val, nil
}

func SumNatural(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if strings.Contains(x, DECIMALPOINT) || strings.Contains(x, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument x: %s, is not a natural number", x)
	}

	if strings.Contains(y, DECIMALPOINT) || strings.Contains(y, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument y: %s, is not a natural number", y)
	}

	s := trimIntegerNil(sum(x, y))

	return s, nil
}

func Difference(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	val, err := deterDiffFunc(x, y)
	if err != nil {
		return NOT_A_NUMBER, err
	}

	return val, nil
}

func DifferenceNatural(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if strings.Contains(x, DECIMALPOINT) || strings.Contains(x, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument x: %s, is not a natural number", x)
	}

	if strings.Contains(y, DECIMALPOINT) || strings.Contains(y, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument y: %s, is not a natural number", y)
	}

	x, y = alignNil(x, y)
	if x == y {
		return NIL, nil
	}

	compare, err := compareAligned(x, y)
	if err != nil {
		return NOT_A_NUMBER, err
	}

	if compare == -1 {
		diff := MINUS + trimIntegerNil(difference(y, x))
		return diff, nil
	}

	return trimIntegerNil(difference(x, y)), nil
}

func Product(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if x == NIL || y == NIL {
		return NIL, nil
	}

	x, y, pos, minus := analyzeProduct(x, y)
	val := trimIntegerNil(product(x, y))
	if pos > 0 {
		val = insertPoint(val, pos)
	}

	if minus && val != NIL {
		val = MINUS + val
	}

	return val, nil
}

func ProductKTS(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if x == NIL || y == NIL {
		return NIL, nil
	}

	x, y, pos, minus := analyzeProduct(x, y)

	val := ktsProduct(x, y)

	if pos > 0 {
		val = insertPoint(val, pos)
	}

	if minus && val != NIL {
		val = MINUS + val
	}

	return val, nil
}

func ProductNatural(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if x == NIL || y == NIL {
		return NIL, nil
	}

	if strings.Contains(x, DECIMALPOINT) || strings.Contains(x, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument x: %s, is not a natural number", x)
	}

	if strings.Contains(y, DECIMALPOINT) || strings.Contains(y, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument y: %s, is not a natural number", y)
	}

	return trimIntegerNil(product(x, y)), nil
}

func ProductKTSNatural(x, y string) (string, error) {
	var e error
	x, y, e = verifyArgument(x, y)
	if e != nil {
		return NOT_A_NUMBER, e
	}

	if x == NIL || y == NIL {
		return NIL, nil
	}

	if strings.Contains(x, DECIMALPOINT) || strings.Contains(x, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument x: %s, is not a natural number", x)
	}

	if strings.Contains(y, DECIMALPOINT) || strings.Contains(y, MINUS) {
		return NOT_A_NUMBER, fmt.Errorf("argument y: %s, is not a natural number", y)
	}

	return ktsProduct(x, y), nil
}
